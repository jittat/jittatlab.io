---
title: "หนังสือ"
date: 2023-02-18T04:48:00+07:00
tags:
- Stories
---

แทบไม่มีมนุษย์เขียนหนังสือกันอีกแล้ว และจริง ๆ แม้กระทั่งเอไอก็แทบจะไม่ได้เขียนสิ่งที่มีลักษณะเป็นหนังสือแบบอดีต 

<!--more-->

การเขียนเนื้อหาขนาดยาวหลายร้อยหน้าเพียงเพื่อจะหวังว่าผลลัพธ์จะได้มีคุณภาพ นั่นคือสร้างความเข้าใจ ความตระหนัก ความบันเทิง หรือความประทับใจให้กับผู้อ่านนั้น ดูจะเป็นการทำงานที่สิ้นเปลืองเกินไป  ระบบเขียนหนังสือจะเขียนแค่หน้าเริ่มต้นไม่กี่หน้า จากนั้นระหว่างที่ผู้อ่านอ่าน ไม่ว่าจะผ่านทางหน้าจอหรือผ่านทางหนังสือเล่มจำลอง (ที่มีเซนเซอร์ตรวจจับมือผู้อ่าน) ทุก ๆ อิริยาบท การกลอกสายตา และการพยักหน้า โดยอาจรวมไปถึงข้อมูลสัญญาณของสมองถ้าผู้อ่านคนนั้นอนุญาตให้เข้าถึงได้ ก็จะถูกนำไปประมวลผลเพื่อสร้างเนื้อหาหน้าถัด ๆ ไปเพื่อผู้อ่านโดยเฉพาะ

ประสบการณ์ของการอ่านหนังสือเล่มหนึ่งจึงแทบจะเป็นประสบการณ์ส่วนตัวที่เฉพาะเจาะจง

แน่นอนถ้าผู้อ่านประทับใจ อาจจะขอเก็บหนังสือเล่มนั้นไว้ได้ และสามารถแบ่งปันให้กับคนอื่น ๆ อ่านได้  ผู้ที่รับไปอ่านก็สามารถเลือกที่จะอ่านหนังสือเล่มนั้นแบบต้นฉบับ หรืออ่านฉบับที่มีการปรับเปลี่ยนให้เข้ากับผู้รับเพื่อสร้างประสบการณ์ที่ใกล้เคียงกันกับคนอ่านคนแรก โดยที่ยังคงโครงเรื่องหลักไว้เช่นเดิม  แน่นอนว่าแทบไม่มีใครยอมทนอ่านหนังสือต้นฉบับได้นานนัก

เมื่อวัฒนธรรมการอ่านเปลี่ยนไปเช่นนี้ การสนทนาเกี่ยวกับหนังสือจึงกลายเป็นกิจกรรมของอดีต พร้อม ๆ กับความสามารถในการแนะนำหนังสือของมนุษย์ด้วยเช่นกัน เพราะว่าหนังสือที่ถูกสร้างขึ้นมามีมากเกินกว่าที่มนุษย์คนใดหรือองค์กรใดจะสามารถติดตามได้

ข้อมูลของหนังสือที่สร้างขึ้นประกอบกับผลลัพธ์ที่ได้ของหนังสือเล่มต่าง ๆ ถูกนำมาป้อนกลับให้ระบบสร้างหนังสือทำงานได้มีประสิทธิภาพและคุณภาพมากขึ้น การอ่านสิ่งที่เคยเรียกว่าหนังสือจึงเป็นกิจกรรมที่ได้รับความนิยมมาก

ระบบปัญญาประดิษฐ์ที่สร้างหนังสือขึ้นระหว่างการอ่านนี้เป็นระบบที่สลับซับซ้อน เต็มไปด้วยข้อมูลมหาศาลรวมทั้งข้อมูลป้อนกลับแบบเวลาจริงที่มีปริมาณมากไปกว่าหนังสือที่มนุษย์เคยเขียนมาทั้งหมดเมื่อนับถึงไม่กี่สิบปีก่อน  ไม่มีมนุษย์คนใดเข้าใจอย่างถ่องแท้ว่าทำไมระบบจึงผลิตข้อความออกมาในแบบที่เป็นในปัจจุบัน และไม่มีมนุษย์คนใดทราบได้ว่าภายใต้ตัวหนังสือเหล่านั้น ระบบอัตโนมัตินี้ได้ใส่ความคิด ทัศนคติ ความเชื่อและความฝันแบบใดให้กับมนุษย์

