---
title: "ทำไม"
date: 1999-12-31T23:59:59+07:00
---
สำหรับรวบรวมอะไรเล็ก ๆ น้อย ๆ ที่ผลิตโดยจิตร์ทัศน์ ฝักเจริญผล

<!--more-->

สมัยเว็บบล็อกรุ่งเรืองเคยเขียนบล็อกเรื่อย ๆ อยู่ที่ [wonam.exteen.com](https://web.archive.org/web/20180306125659/http://wonam.exteen.com/) ที่ตอนนี้ต้องไปดูที่เว็บเก็บอดีต และมีเขียนเก็บไว้ประปรายที่ [jittat.wordpress.com](https://jittat.wordpress.com/) มีช่วงหนึ่งพยายามลองไปเขียนและนำบทความเก่า ๆ ไปเก็บไว้ที่ที่ [Medium](https://medium.com/@jittat)

วลีว่า "ท้องฟ้าไม่มีความหมาย" มาจากหัวบล็อกเก่าที่ exteen ที่เขียนไว้ว่า "ท้องฟ้าไม่มีความหมาย ถ้าไม่มีใครแหงนมองฟ้าของคนอื่น" ประโยคดังกล่าวดังขึ้นในวันหนึ่งในอดีตที่ลบบทความเดิมที่เขียนในบล็อกทิ้งทั้งหมด ก่อนที่จะเขียนเรื่องเกี่ยวกับประเด็นความรุนแรงในภาคใต้ เรื่องที่เขียนนั้นอยู่ไหนแล้วก็ไม่ทราบ น่าจะเกี่ยวข้องกับบทความจากหนังสือพิมพ์ที่พูดเกี่ยวกับความจริงและความลวงทางประวัติศาสตร์

ลิงก์กันลืม
-------

รวมโพสที่อยากจะเอามาใส่ แต่ยังไม่มีเวลา

* 15 ม.ค. 2022 [โอกาสในความล้มเหลวที่ไม่เท่ากัน](https://www.facebook.com/jittat/posts/pfbid0d4aaYij3KQiHs3VgLVhoFtMSwvt42cq52wHdCCwPzHFom6C8oXn9t2V6vdTjo8u8l)
* 6 มี.ค. 2023 [Dynabook](https://www.facebook.com/jittat/posts/pfbid033b9evgeGffWok9iCV3Kv9HLfhMQrqTYJGADMTw8QphTgc6BHFKmF5PsG2vtg5Nhyl)
* 19 มี.ค. 2023 [Ramsey's theorem (ภาค 1)](https://www.facebook.com/jittat/posts/pfbid035sQL4szF5nW6pCx23wb46pi4wDEG4KZTM2eC8QmKcW1GSYouBLHLY1XsHACaGKmCl)
* 28 มี.ค. 2023 [ยังมีอะไรที่เราไม่รู้อีกมากมายเกี่ยวกับ Large Language Model](https://www.facebook.com/jittat/posts/pfbid02jqjccLWdrkknv27mpqzVsTKNsVYVexeBGULTgEwLY1aTSDyQZgkdXGTYqp7NV1nEl)
* 12 เม.ย. 2023 [ระหว่างที่ตรวจข้อสอบ (ยังไม่เสร็จ)](https://www.facebook.com/jittat/posts/pfbid0244LuH8qDRZrUiGCEezy77hYLgQ95cWPWiqSNV9R9ZHBgnhgYyVpr9FZ4uJr4HREBl)